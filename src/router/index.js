import { h, resolveComponent } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import store from '../store'

import DefaultLayout from '@/layouts/DefaultLayout'

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/pages/Login'),
  },

  {
    path: '/',
    name: 'AnaSayfa',
    component: DefaultLayout,
    redirect: '/dashboard',
    children: [
      {
        path: '/news',
        name: 'Haberler',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/news/list',
        children: [
          {
            path: '/news/list',
            name: 'Haber Listesi',
            component: () => import('@/views/news/NewsList.vue'),
          },
          {
            path: '/news/create',
            name: 'Haber Ekleme',
            component: () => import('@/views/news/NewsCreate.vue'),
          },
        ],
      },

      {
        path: '/announcements',
        name: 'Duyurular',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/announcements/list',
        children: [
          {
            path: '/announcements/list',
            name: 'Duyuru Listesi',
            component: () =>
              import('@/views/announcements/AnnouncementList.vue'),
          },
          {
            path: '/announcements/create',
            name: 'Duyuru Ekleme',
            component: () =>
              import('@/views/announcements/AnnouncementCreate.vue'),
          },
        ],
      },

      {
        path: '/activities',
        name: 'Etkinllikler',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/activities/list',
        children: [
          {
            path: '/activities/list',
            name: 'Etkinlik Listesi',
            component: () => import('@/views/activities/ActivityList.vue'),
          },
          {
            path: '/activities/create',
            name: 'Etkinlik Oluştur',
            component: () => import('@/views/activities/ActivityCreate.vue'),
          },
        ],
      },

      {
        path: '/photo-galleries',
        name: 'Foto Galeri',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/photo-galleries/list',
        children: [
          {
            path: '/photo-galleries/list',
            name: 'Foto Galeri Listesi',
            component: () =>
              import('@/views/photoGalleries/PhotoGalleryList.vue'),
          },
          {
            path: '/photo-galleries/create',
            name: 'Fotoğraf Ekleme',
            component: () =>
              import('@/views/photoGalleries/PhotoGalleryCreate.vue'),
          },
        ],
      },

      {
        path: '/videos',
        name: 'Videolar',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/videos/list',
        children: [
          {
            path: '/videos/list',
            name: 'Video Listesi',
            component: () => import('@/views/videos/VideoList.vue'),
          },
          {
            path: '/videos/create',
            name: 'Video Ekle',
            component: () => import('@/views/videos/VideoCreate.vue'),
          },
        ],
      },

      {
        path: '/magazines',
        name: 'E-Bültenler',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/magazines/list',
        children: [
          {
            path: '/magazines/list',
            name: 'E-Bülten Listesi',
            component: () => import('@/views/magazines/MagazineList.vue'),
          },
          {
            path: '/magazines/create',
            name: 'E-Bülten Ekle',
            component: () => import('@/views/magazines/MagazineCreate.vue'),
          },
          {
            path: '/magazines/:id',
            name: 'E-Bülten Detay',
            component: () => import('/src/views/magazines/MagazineShow.vue'),
          },
        ],
      },

      {
        path: '/referances',
        name: 'Referanslar',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/referances/list',
        children: [
          {
            path: '/referances/list',
            name: 'Referans Listesi',
            component: () => import('@/views/referances/ReferanceList.vue'),
          },
          {
            path: '/referances/create',
            name: 'Referans Ekle',
            component: () => import('@/views/referances/ReferanceCreate.vue'),
          },
        ],
      },

      {
        path: '/pages',
        name: 'Sayfalar',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/pages/list',
        children: [
          {
            path: '/pages/list',
            name: 'Sayfa Listesi',
            component: () => import('/src/views/extraPages/PageList.vue'),
          },
          {
            path: '/pages/create',
            name: 'Sayfa Ekle',
            component: () => import('/src/views/extraPages/PageCreate.vue'),
          },
        ],
      },

      {
        path: '/icons',
        name: 'Simgeler',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/icons/list',
        children: [
          {
            path: '/icons/list',
            name: 'Simge Listesi',
            component: () => import('/src/views/extraIcons/IconList.vue'),
          },
          {
            path: '/icons/create',
            name: 'Simge Ekle',
            component: () => import('/src/views/extraIcons/IconCreate.vue'),
          },
        ],
      },

      {
        path: '/billboards',
        name: 'Bilgi Panosu',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/billboards/list',
        children: [
          {
            path: '/billboards/list',
            name: 'Pano Listesi',
            component: () => import('/src/views/billboards/BillboardList.vue'),
          },
          {
            path: '/billboards/create',
            name: 'Pano Oluştur',
            component: () =>
              import('/src/views/billboards/BillboardCreate.vue'),
          },
        ],
      },

      {
        path: '/modals',
        name: 'Açılır Pencere',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/modals/list',
        children: [
          {
            path: '/modals/list',
            name: 'Açılır Pencereleri Listele',
            component: () => import('/src/views/modals/ModalList.vue'),
          },
          {
            path: '/modals/create',
            name: 'Açılır Pencere Ekleme',
            component: () => import('/src/views/modals/ModalCreate.vue'),
          },
        ],
      },

      {
        path: '/top-menus',
        name: 'Üst Menüler',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/top-menus/list',
        children: [
          {
            path: '/top-menus/list',
            name: 'Üst Menü Listele',
            component: () => import('/src/views/topMenus/TopMenuList.vue'),
          },
          {
            path: '/top-menus/create',
            name: 'Üst Menü Ekle',
            component: () => import('/src/views/topMenus/TopMenuCreate.vue'),
          },
        ],
      },

      {
        path: '/mid-menus',
        name: 'Orta Menüler',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/mid-menus/list',
        children: [
          {
            path: '/mid-menus/list',
            name: 'Orta Menü Listele',
            component: () => import('/src/views/midMenus/MidMenuList.vue'),
          },
          {
            path: '/mid-menus/create',
            name: 'Orta Menü Ekle',
            component: () => import('/src/views/midMenus/MidMenuCreate.vue'),
          },
        ],
      },

      {
        path: '/header-menus',
        name: 'Ana Menü',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/header-menus/list',
        children: [
          {
            path: '/header-menus/list',
            name: 'Ana Menü Listesi',
            component: () =>
              import('/src/views/headerMenus/HeaderMenuList.vue'),
          },
          {
            path: '/header-menus/create',
            name: 'Ana Menü Ekle',
            component: () =>
              import('/src/views/headerMenus/HeaderMenuCreate.vue'),
          },
        ],
      },

      {
        path: '/header-menu-items',
        name: 'Ana Menü İçerikleri',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/header-menu-items/list',
        children: [
          {
            path: '/header-menu-items/list',
            name: 'Ana Menü İçerikler Listesi',
            component: () =>
              import('/src/views/headerMenuItems/HeaderMenuItemList.vue'),
          },
          {
            path: '/header-menu-items/create',
            name: 'Ana Menü İçerik Ekle',
            component: () =>
              import('/src/views/headerMenuItems/HeaderMenuItemCreate.vue'),
          },
        ],
      },

      {
        path: '/footer-menus',
        name: 'Alt Menü',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/footer-menus/list',
        children: [
          {
            path: '/footer-menus/list',
            name: 'Alt Menü Listesi',
            component: () =>
              import('/src/views/footerMenus/FooterMenuList.vue'),
          },
          {
            path: '/footer-menus/create',
            name: 'Alt Menü Ekle',
            component: () =>
              import('/src/views/footerMenus/FooterMenuCreate.vue'),
          },
        ],
      },

      {
        path: '/footer-menu-items',
        name: 'Alt Menü İçerik',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/footer-menu-items/list',
        children: [
          {
            path: '/footer-menu-items/list',
            name: 'Alt Menü İçerik Listesi',
            component: () =>
              import('/src/views/footerMenuItems/FooterMenuItemList.vue'),
          },
          {
            path: '/footer-menu-items/create',
            name: 'Alt Menü İçerik Ekle',
            component: () =>
              import('/src/views/footerMenuItems/FooterMenuItemCreate.vue'),
          },
        ],
      },

      {
        path: '/services',
        name: 'Hizmetler',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/services/list',
        children: [
          {
            path: '/services/list',
            name: 'Hizmetleri Listele',
            component: () => import('/src/views/services/ServiceList.vue'),
          },
          {
            path: '/services/create',
            name: 'Hizmet Ekle',
            component: () => import('/src/views/services/ServiceCreate.vue'),
          },
        ],
      },

      {
        path: '/menus',
        name: 'Hızlı Menü',
        component: {
          render() {
            return h(resolveComponent('router-view'))
          },
        },
        redirect: '/menus/list',
        children: [
          {
            path: '/menus/list',
            name: 'Hızlı Menü Listele',
            component: () => import('/src/views/menus/MenuList.vue'),
          },
          {
            path: '/menus/create',
            name: 'Hızlı Menü Ekle',
            component: () => import('/src/views/menus/MenuCreate.vue'),
          },
          {
            path: '/menus/:id',
            name: 'Menü Güncelle',
            component: () => import('/src/views/menus/MenuShow.vue'),
          },
        ],
      },

      {
        path: '/dashboard',
        name: 'Gösterge Paneli',
        component: () =>
          import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard.vue'),
      },
    ],
  },

  // {
  //   path: '/pages',
  //   redirect: '/pages/404',
  //   name: 'Pages',
  //   component: {
  //     render() {
  //       return h(resolveComponent('router-view'))
  //     },
  //   },
  //   children: [
  //     {
  //       path: '404',
  //       name: 'Page404',
  //       component: () => import('@/views/pages/Page404'),
  //     },
  //     {
  //       path: '500',
  //       name: 'Page500',
  //       component: () => import('@/views/pages/Page500'),
  //     },
  //     {
  //       path: 'login',
  //       name: 'Login',
  //       component: () => import('@/views/pages/Login'),
  //     },
  //     {
  //       path: 'register',
  //       name: 'Register',
  //       component: () => import('@/views/pages/Register'),
  //     },
  //   ],
  // },
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
  scrollBehavior() {
    // always scroll to top
    return { top: 0 }
  },
})

router.beforeEach((to, from, next) => {
  const publicPages = ['Login']
  const authRequired = !publicPages.includes(to.name)
  const loggedIn = store.getters._isAuthenticated

  if (authRequired && !loggedIn) {
    next({ name: 'Login' })
  } else {
    next()
  }
})

export default router
