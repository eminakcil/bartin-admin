import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import SecureLS from 'secure-ls'

const ls = new SecureLS({ isCompression: false })

export default createStore({
  state: {
    user: null,
    sidebarVisible: '',
    sidebarUnfoldable: false,
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    logoutUser(state) {
      state.user = null
    },
    toggleSidebar(state) {
      state.sidebarVisible = !state.sidebarVisible
    },
    toggleUnfoldable(state) {
      state.sidebarUnfoldable = !state.sidebarUnfoldable
    },
    updateSidebarVisible(state, payload) {
      state.sidebarVisible = payload.value
    },
  },
  getters: {
    _isAuthenticated: (state) => state.user !== null,
    _getCurrentUser: (state) => state.user,
    _currentUserId: (state) => state?.user?._id,
  },
  actions: {},
  modules: {},
  plugins: [
    createPersistedState({
      storage: {
        getItem: (key) => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: (key) => ls.remove(key),
      },
    }),
  ],
})
