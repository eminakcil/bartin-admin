import store from '../store'

export default function authHeader() {
  let user = store.getters._getCurrentUser

  if (user && user?.access_token) {
    return 'Bearer ' + user.access_token
  } else {
    return false
  }
}
