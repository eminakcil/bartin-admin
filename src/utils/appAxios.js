import axios from 'axios'
import authHeader from './authHeader'
import store from '../store'
import router from '../router'

const appAxios = axios.create({
  baseURL: `${process.env.VUE_APP_API_HOST}/${process.env.VUE_APP_API_PATH}`,
})

appAxios.interceptors.request.use((config) => {
  const accessToken = authHeader()
  if (accessToken) config.headers.Authorization = accessToken

  return config
})

appAxios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response.status === 401) {
      store.commit('logoutUser')
      router.push({ name: 'Login' })
    } else {
      throw error
    }
  },
)

export default appAxios
