const generateNavLinks = (name, suffix, mainRoute, icon = 'cil-puzzle') => {
  return {
    component: 'CNavGroup',
    name: `${name}${suffix}`,
    to: `/${mainRoute}`,
    icon,
    items: [
      {
        component: 'CNavItem',
        name: `${name} Ekle`,
        to: `/${mainRoute}/create`,
      },
      {
        component: 'CNavItem',
        name: `${name} Listele`,
        to: `/${mainRoute}/list`,
      },
    ],
  }
}

export default [
  {
    component: 'CNavItem',
    name: 'Anasayfa',
    to: '/dashboard',
    icon: 'cilHome',
    badge: {
      color: 'primary',
    },
  },

  generateNavLinks('Haber', 'ler', 'news', 'cilNewspaper'),
  generateNavLinks('Duyuru', 'lar', 'announcements', 'cilHeadphones'),
  generateNavLinks('Etkinlik', 'ler', 'activities', 'cilNotes'),
  generateNavLinks('Fotogaleri', '', 'photo-galleries', 'cilFile'),
  generateNavLinks('Video', 'lar', 'videos', 'cilVideo'),
  generateNavLinks('E-Bülten', 'ler', 'magazines'),
  generateNavLinks('Referans', 'lar', 'referances', 'cilUser'),
  generateNavLinks('Sayfa', 'lar', 'pages', 'cilPencil'),
  generateNavLinks('Simge', 'ler', 'icons'),
  generateNavLinks('Bilgi Panosu', '', 'billboards', 'cilLocationPin'),
  generateNavLinks('Açılır Pencere', '', 'modals', 'cilNotes'),
  generateNavLinks('Üst Menü', '', 'top-menus', 'cilList'),
  generateNavLinks('Orta Menü', '', 'mid-menus', 'cilOptions'),
  generateNavLinks('Header Menü', '', 'header-menus'),
  // generateNavLinks('Header Menü Öğe', '', 'header-menu-items'),
  generateNavLinks('Footer Menü', '', 'footer-menus'),
  generateNavLinks('Footer Menü Öğe', '', 'footer-menu-items'),
  generateNavLinks('Hizmet', 'ler', 'services', 'cilTask'),
  generateNavLinks('Hızlı Menü', 'ler', 'menus', 'cilMenu'),
]
