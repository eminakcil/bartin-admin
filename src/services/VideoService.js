import { appAxios } from '@/utils'

class VideoService {
  static getAll() {
    return appAxios.get('/videogaleris/getAll')
  }

  static create(data) {
    return appAxios.post('/videogaleris/add', {
      Baslik: data.title,
      Dizin: data.link,
    })
  }
  static delete(id) {
    id = parseInt(id)
    return appAxios.post(`/Videogaleris/deletedto?id=${id}`)
  }
}

export default VideoService
