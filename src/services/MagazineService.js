import { appAxios } from '@/utils'

class MagazineService {
  static getAll() {
    return appAxios.get('/e_bultenlers/getall')
  }

  static getById(id) {
    return appAxios.get(`/e_bultenlers/getbyid`, { params: { id } })
  }

  static create(data) {
    const formData = new FormData()

    formData.append('Ad', data.title)
    formData.append('Baglanti', data.link)
    formData.append('Tarih', data.date)
    formData.append('image', data.image)

    return appAxios.post('/e_bultenlers/addwithimage', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
  }
  static delete(id) {
    id = parseInt(id)
    return appAxios.post(`/e_bultenlers/deletedto?id=${id}`)
  }
  static update(id, data) {
    const formData = new FormData()

    formData.append('Id', parseInt(id))
    formData.append('Ad', data.title)
    formData.append('Baglanti', data.link)
    formData.append('Tarih', data.date)
    if (data.image) formData.append('image', data.image)

    return appAxios.post('/e_bultenlers/update', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    })
  }
}

export default MagazineService
