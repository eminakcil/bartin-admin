import { appAxios } from '@/utils'

class MenuService {
  static path = 'menus'

  static getAll() {
    return appAxios.get(`${this.path}/getall`)
  }

  static getById(id) {
    return appAxios.get(`${this.path}/getbyid`, { params: { id } })
  }

  static create(menus) {
    return appAxios.post(`${this.path}/add`, {
      Ad: menus.title,
      Baglanti: menus.link,
      Aktiflik: true,
    })
  }

  static delete(id) {
    id = parseInt(id)
    return appAxios.post(`/Menus/deletedto?id=${id}`)
  }

  static update(id, data) {
    return appAxios.post(`${this.path}/update`, {
      Id: parseInt(id),
      Ad: data.title,
      Baglanti: data.link,
    })
  }
}
export default MenuService
