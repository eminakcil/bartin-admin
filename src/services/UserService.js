import { appAxios } from '@/utils'

class UserService {
  static path = 'kullanicis'

  static login(data) {
    return appAxios.post(
      `/${this.path}/authenticate`,
      {
        mail: data.email,
        sifre: data.password,
      },
      {
        headers: {
          'Content-Type': 'application/json',
        },
      },
    )
  }
}

export default UserService
